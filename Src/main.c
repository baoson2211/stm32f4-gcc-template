#include <stdio.h>
#include <stdint.h>

#include "main.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_it.h"

GPIO_InitTypeDef  GPIO_InitStructure;
NVIC_InitTypeDef  NVIC_InitStructure;
EXTI_InitTypeDef  EXTI_InitStructure;
RCC_ClocksTypeDef RCC_Clocks;

static __IO uint32_t uwTimingDelay;
__IO uint8_t  mode;

void EXTI_config(void);

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{
  uwTimingDelay = nTime;

  while(uwTimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (uwTimingDelay != 0x00)
  {
    uwTimingDelay--;
  }
}

/*
 * main()
 */
int main(void)
{
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);

    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;  // enable the clock to GPIOD
    __asm("dsb");                         // stall instruction pipeline, until instruction completes, as
                                          //    per Errata 2.1.13, "Delay after an RCC peripheral clock enabling"
    GPIOD->MODER = (1 << 24)|             // set pin 12 to be general purpose output
                   (1 << 26)|             // set pin 13 to be general purpose output
                   (1 << 28)|             // set pin 14 to be general purpose output
                   (1 << 30);             // set pin 15 to be general purpose output

    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    EXTI_config();

    while (1) {
        Delay(100);
        if (!mode) GPIOD->ODR |= (1 << 13);           // Set the pin 13
        Delay(100);
        if (!mode) GPIOD->ODR |= (1 << 14);           // Set the pin 14
        Delay(100);
        if (!mode) GPIOD->ODR |= (1 << 15);           // Set the pin 15
        Delay(100);
        if (!mode) GPIOD->ODR |= (1 << 12);           // Set the pin 12
        Delay(100);
        if (!mode) GPIOD->ODR &=~(1 << 13);           // Reset the pin 13
        Delay(100);
        if (!mode) GPIOD->ODR &=~(1 << 14);           // Reset the pin 14
        Delay(100);
        if (!mode) GPIOD->ODR &=~(1 << 15);           // Reset the pin 15
        Delay(100);
        if (!mode) GPIOD->ODR &=~(1 << 12);           // Reset the pin 12

        /*
        Delay(250);
        GPIOD->ODR &=~((1 << 12)|
                       (1 << 13)|
                       (1 << 14)|
                       (1 << 15));         // Reset  the pins
        */
    }
}

void EXTI_config() {
    /* Button PA0 */
    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType= GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed= GPIO_Speed_100MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Configure EXTI Line0 */
    EXTI_InitStructure.EXTI_Line = EXTI_Line0;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable and set EXTI Line0 Interrupt to the lowest priority */
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

