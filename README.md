# README #

My template project for STM32F4 Discovery Board.
Using SPL, auto build with make and require arm-none-eabi-gcc-4.8.4 or newer.

### Tree ###
```
.
├── Libraries     => Standard peripheral libraries
├── Inc           => user header files
├── Src           => user source files
└── Utilities     => third party libraries
```