#
# Simple makefile to compile the example code given for the STM32F10x
# family of ARM processors. This file should be placed in the same
# directory that you wish to compile.
#
# INCLUDEDIRS selects all folders in the package that have header files. Since
#             this is already an inclusive list, it should not need editing.
#
# LIBSOURCES contains all the Driver (SPL/HAL/BSP) files that need to be compiled
#            for this specific project. This should be updated with the help
#            of the dependencies file (main.d) generated on any (including
#            unsucessful) compilation.
#
# Note: The provided code sometimes uses windows backslashes (\) in include
#       paths. Update as needed.
#
# Note: When building the lists for LIBSOURCES, *_ex.c files must come before
#       the generic version of the file. This is necessary for linking to find
#       the extended (board-specific) version of the function.
#

# build environment
CC      = arm-none-eabi-gcc
CXX     = arm-none-eabi-g++
AR      = arm-none-eabi-ar
AS      = arm-none-eabi-as
OBJCOPY = arm-none-eabi-objcopy
OBJDUMP = arm-none-eabi-objdump
SIZE    = arm-none-eabi-size
MAKE    = make

# location of OpenOCD Board .cfg files (only used with 'make program')
OPENOCD_BOARD_DIR=/usr/local/share/openocd/scripts/board

# Configuration (cfg) file containing programming directives for OpenOCD
OPENOCD_PROC_FILE=stm32f4discovery.cfg

# project parameters
PROJ_NAME = demo-stm32f4
CPU_FAMILY = STM32F4XX
CPU_MODEL_GENERAL = STM32F40XX
CPU_MODEL_SPECIFIC = STM32F407VG
OUTPATH = .

################################################################################
# Libraries
# Header of all libraries
INCLUDEDIRS  = Inc
INCLUDEDIRS += Libraries/STM32F4xx_StdPeriph_Driver/inc
INCLUDEDIRS += Libraries/CMSIS/Device/ST/STM32F4xx/Include
INCLUDEDIRS += Libraries/CMSIS/RTOS/Template Libraries/CMSIS/Include

# Source of specify libraries
LIBSOURCES  =
#LIBSOURCES += Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates/system_stm32f4xx.c
#LIBSOURCES += Libraries/STM32F4xx_StdPeriph_Driver/src/

LIBOBJS = $(LIBSOURCES:.c=.o)

################################################################################
# Source files
#
# auto-generated project paths
SOURCES  = $(shell find Src -name *.c)
# system_stm32f40x.c
SOURCES += $(shell find Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates -name *.c)
# full std periph libraries - if you wanna choose specify libraries, comment the line below
# and define it in LIBSOURCES
ifeq ($(CPU_MODEL_GENERAL), STM32F427_437xx)
	SOURCES += $(shell find Libraries/STM32F4xx_StdPeriph_Driver/src -type f \( -iname "*.c" ! -iname "*ltdc.c" ! -iname "*rng.c" \
							! -iname "*dac.c" \) )
else ifeq ($(CPU_MODEL_GENERAL), STM32F429_439xx)
	SOURCES += $(shell find Libraries/STM32F4xx_StdPeriph_Driver/src -type f \( -iname "*.c" ! -iname "*rng.c" ! -iname "*dac.c" \) )
else ifeq ($(CPU_MODEL_GENERAL), STM32F446xx)
	SOURCES += $(shell find Libraries/STM32F4xx_StdPeriph_Driver/src -type f \( -iname "*.c" ! -iname "*rng.c" ! -iname "*dac.c" \) )
else ifeq ($(CPU_MODEL_GENERAL), STM32F469_479xx)
	SOURCES += $(shell find Libraries/STM32F4xx_StdPeriph_Driver/src -type f \( -iname "*.c" ! -iname "*rng.c" ! -iname "*dac.c" \) )
else
	SOURCES += $(shell find Libraries/STM32F4xx_StdPeriph_Driver/src -type f \( -iname "*.c" ! -iname "*rng.c" ! -iname "*fmc.c" \
							! -iname "*sai.c" ! -iname "*dac.c" \) )
endif
SOURCES += Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates/TrueSTUDIO/startup_stm32f40xx.s

# C Object files
OBJC = $(SOURCES:.c=.o)
# add ASM Object files
OBJ = $(OBJC:.s=.o)

# Linker file
# can see it in STM32F4xx_DSP_StdPeriph_Lib_V1.x.x/Project/STM32F4xx_StdPeriph_Templates/TrueSTUDIO/STM32F4xxxxx
# X depend on your mcu
LDSCRIPT = -Wl,-T stm32_flash.ld

# CFLAGS
CFLAGS  = -Wall -g -O2 -D $(CPU_MODEL_GENERAL)        # Normal
#CFLAGS = -Wall -ggdb -O0  -D $(CPU_MODEL_GENERAL)    # RSW - for GDB debugging, disable optimizer
CFLAGS += -mlittle-endian -mthumb -mthumb-interwork -mcpu=cortex-m4
CFLAGS += -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += -Wl,--gc-sections -Wl,-Map=$(PROJ_NAME).map
CFLAGS +=  $(addprefix -I ,$(INCLUDEDIRS))

.PHONY: all

all: proj

proj: $(PROJ_NAME).elf

################################################################################
# Compile and assemble sequence C files, but do not link
#
%.o: %.c
# $(eval AUTOINCLUDES = $(addprefix -include ,$(shell find $(dir $<) -name *.h)))
	$(CC) -c -o $@ $< $(CFLAGS)

# test:
#	$(eval AUTOINCLUDES = $(addprefix -include ,$(shell find $(dir $<) -name *.h)))
#	echo $(AUTOINCLUDES)

################################################################################
# A brief introduction about the libraries:
#
# -lc      : link to standard C library ( stdio.h and stdlib.h are examples)
# -lg      : a debugging-enabled libc
# -lm      : link to math C library ( math.h )
# -lnosys  : non-semihosting
#            link to libnosys.a - The libnosys.a is used to satisfy all system call references,
#            although with empty calls. In this configuration, the debug output
#            is forwarded to the semihosting debug channel, vis SYS_WRITEC.
#            The application and redefine all syscall implementation functions, like _write(),
#            _read(), etc. When using libnosys.a, the startup files are not needed
#            Since 4.8, recommended -specs=nosys.specs instead
# -lrdimon : semihosting
#            link to librdimon.a - implements all system calls via the semihosting API with all
#            functionality provided by the host. When using librdimon.a, the startup files are
#            required to provide all specific initialisation, and the rdimon.specs
#            must be added to the linker
#            Since 4.8, recommended -specs=rdimon.specs instead
#

################################################################################
# Link all object files
# -l : library search, applies in a special way to libraries used with the linker
#      (GNU Make - page 28 - 4.4.6 Directory Search for Link Libraries)
# -L : Extra flags to give to compilers when they are supposed to invoke the linker, ‘ld’, such as -L.
#      Libraries (-lfoo) should be added to the LDLIBS variable instead.
#      (GNU Make - page 72 - 6.13 Suppressing Inheritance)
# -T : ld script file
#
$(PROJ_NAME).elf: $(SOURCES) $(LIBOBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(LDSCRIPT) --specs=nano.specs --specs=rdimon.specs -lc -lm -lrdimon
	$(OBJCOPY) -O ihex $(PROJ_NAME).elf $(PROJ_NAME).hex
	$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin
	$(OBJDUMP) -St $(PROJ_NAME).elf > $(PROJ_NAME).lst
	$(SIZE) $(PROJ_NAME).elf

################################################################################
# Programming with ST-link v2
#
burn: proj
	$(STLINK)/st-flash write $(OUTPATH)/$(PROJ_NAME).bin 0x8000000

################################################################################
# Clean
#
clean:
	rm -f $(PROJ_NAME).elf
	rm -f $(PROJ_NAME).hex
	rm -f $(PROJ_NAME).bin
	rm -f $(PROJ_NAME).map
	rm -f $(PROJ_NAME).lst
	rm -f *.o

